import React from "react";
import { Container, Row } from "react-bootstrap";
import DetailInfo from "../DetailInfo/";
import DetailActions from "../DetailActions";

interface Props {
  city: string;
  name: string;
  text: string;
  aboutme: string;
  languages: string;
  activities: boolean;
  review: number;
  star: number;
}

function Detail(props: Props) {
  return (
    <Container>
      <Row>
        <DetailInfo
          city={props.city}
          name={props.name}
          text={props.text}
          aboutme={props.aboutme}
          languages={props.languages}
          activities={props.activities}
        />
        <DetailActions
          name={props.name}
          city={props.city}
          review={props.review}
          star={props.star}
        />
      </Row>
    </Container>
  );
}

export default Detail;
