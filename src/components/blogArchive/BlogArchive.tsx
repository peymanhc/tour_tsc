import { makeStyles } from "@material-ui/styles";
import React from "react";
import Brands from "./Brands";
import NewUsers from "./NewUsers";
import Post from "./Post";

const useStyles = makeStyles({
  title: {
    fontSize: 18,
    fontWeight: 700,
    marginBottom: 20,
  },
  imgprofile: {
    borderRadius: "50%",
    width: 60,
    height: 60,
    objectFit: "cover",
  },
  decription: {
    maxHeight: 45,
    fontSize: 12,
    color: "rgba(0,0,0,0.4)",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 2,
    "-webkit-box-orient": "vertical",
  },
  checked: {
    color: "orange",
  },
});
interface Props {}
const BlogArchive = (props: Props) => {
  const styles = useStyles();
  return (
    <div className="mt-5 p-4">
      <h2 className={styles.title}>Recently booked locals</h2>
      <NewUsers />
      <NewUsers />
      <NewUsers />
      <div className="d-flex justify-content-between">
        <Brands bgcolor="#0075bb" icon="fa fa-facebook" />
        <Brands bgcolor="#f1255e" icon="fa fa-instagram" />
        <Brands bgcolor="#02c9ff" icon="fa fa-twitter" />
        <Brands bgcolor="#cf3427" icon="fa fa-youtube" />
        <Brands bgcolor="#63b4e4" icon="fa fa-telegram" />
      </div>
      <h2 className={`${styles.title} mt-4`}>New Posts</h2>
      <Post/>
      <Post/>
    </div>
  );
};

export default BlogArchive;
