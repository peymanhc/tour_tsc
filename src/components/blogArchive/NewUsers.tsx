import { makeStyles } from '@material-ui/styles';
import React from 'react'

const useStyles = makeStyles({
    title: {
      fontSize: 18,
      fontWeight: 700,
      marginBottom: 20,
    },
    imgprofile: {
      borderRadius: "50%",
      width: 60,
      height: 60,
      objectFit: "cover",
    },
    decription: {
      maxHeight: 45,
      fontSize: 12,
      color: "rgba(0,0,0,0.4)",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": 2,
      "-webkit-box-orient": "vertical",
    },
    checked: {
      color: "orange",
    },
  });
interface Props {
    
}

const NewUsers = (props: Props) => {
    const styles = useStyles();
    return (
        <div className="d-flex">
        <img
          className={styles.imgprofile}
          alt="profile"
          src="https://showaroundoriginal.imgix.net/sa/9579727/fu1544386951xlkjeii3su.jpg?w=64&h=64&dpr=1.5&or=0&q=60&rect=0,0,805,805"
        />
        <div className="pl-2">
          <h6 className="font-weight-bold d-flex">
            Diana
            <span className="ml-2" >
              <span className={`fa fa-star ${styles.checked}`}></span>
              <span className={`fa fa-star ${styles.checked}`}></span>
              <span className={`fa fa-star ${styles.checked}`}></span>
              <span className="fa fa-star"></span>
              <span className="fa fa-star"></span>
            </span>
          </h6>
          <p className={styles.decription}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem
            pariatur iste cumque sapiente eaque, rem, est a nisi soluta fuga
            libero distinctio nostrum? Quas magnam fugiat repellat quo nesciunt
            nostrum!
          </p>
        </div>
      </div>
    )
}

export default NewUsers
