import React from 'react';

const Leaderimg: React.FC = (props) => {
    return (
        <img alt="profile" src={require('../../images/profile.jpg')} />
    )
};
export default Leaderimg;