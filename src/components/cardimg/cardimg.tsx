import React from "react";
import { Col } from "react-bootstrap";

import styles from "../card/card.module.scss";

const Cardimg: React.FC = () => {
  return (
    <Col>
      <img
        alt="profileimg"
        className={styles.imgCard}
        src={require("../../images/profile.jpg")}
      />
    </Col>
  );
};

export default Cardimg;
