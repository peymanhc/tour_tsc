import React, { ReactElement } from "react";

import Header from "../../components/RegisterHeader";
import RegisterBody from "../../components/RegisterBody";
import RegisterForm from "../../components/RegisterForm";

function Register(): ReactElement {
  return (
    <div>
      <Header />
      <RegisterBody />
      <RegisterForm />
    </div>
  );
}

export default Register;
