import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Col, Container, Row } from "react-bootstrap";
import logo from "../../images/logo.png";
import Blogpost from "../../components/blogPost/Blogpost";
import BlogArchive from "../../components/blogArchive/BlogArchive";
const useStyles = makeStyles({
  head: {
    width: "100%",
    height: 70,
    background: "#bdc1ad",
  },
  headtext: {
    display: "flex",
    height: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    color: "white",
    fontSize: 25,
  },
});

interface Props {}

const Blog = (props: Props) => {
  const styles = useStyles();
  return (
    <>
      <div className={styles.head}>
        <Container className={styles.headtext}>
          <p className="mb-0">
            The ultimate source for travel inspiration and insider tips by
            locals
          </p>
          <img className="mt-2" alt="logo" src={logo} />
        </Container>
      </div>
      <Container>
        <Row>
          <Col className="mt-5" md="8">
          <Blogpost />
          <Blogpost />
          </Col>
         <Col  md="4">
         <BlogArchive />
         </Col>
        </Row>
      </Container>
    </>
  );
};

export default Blog;
