import React, { ReactElement } from "react";
import { Container, Row } from "react-bootstrap";
import Settings from "../../components/settings";
import SettingsInfo from "../../components/settingsInfo";

function ProfileSettings(): ReactElement {
  return (
    <Container className="mt-5">
      <Row>
        <Settings />
        <SettingsInfo />
      </Row>
    </Container>
  );
}

export default ProfileSettings;
